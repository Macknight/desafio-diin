package hello;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RentabilidadeController {

    private static final int diasPorSemana = 5;
    private static final double rendimento = 0.064;
    private static final int diasUteisNoAno = 252;

    /*
    * API ENDPOINT
    */
    @GetMapping("/rentabilidade")
    public String rentabilidade(@RequestParam(name="name", required=false, defaultValue="Joao") String name, 
	@RequestParam(name="time", required=false, defaultValue="36") String time,
	@RequestParam(name="contribution", required=false, defaultValue="100") String contribution,
	Model model) {
        //parsing parameters to numbers
        int timeInWeeks = Integer.parseInt(time);			
        double weeklyContribution = Double.parseDouble(contribution);
        //calculate gains with weekly contribution
        double finalMoneyInDaPocket = calculateGainsWeeklyContribution(weeklyContribution, timeInWeeks);
        //set up for display at two decimal places
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);

        model.addAttribute("name", name);
        model.addAttribute("moneys",round(finalMoneyInDaPocket,2));
        model.addAttribute("time", time);
        model.addAttribute("contribution", contribution);

        return "greeting.html";
    }
    /*
    * INDEX PAGE ENDPOINT
    */
	@GetMapping("/")
    public String index( Model model) {
        return "index.html";
    }
    /*
    * CalculateGains when the user adds a weekly amount to the account
    * @param weeklyContribution -> amount of moneyz per week 
    * @param weeks -> number of weeks awaiting for moneyz to increase
    */
    public static double calculateGainsWeeklyContribution(double weeklyContribution, int weeks){

        double patrimonio = 0;
        while (weeks > 0 ){
            patrimonio += calculateGains(weeklyContribution, weeks);
            weeks--;
            System.out.println("patrimonio = " + patrimonio);
            System.out.println("weeks = " + weeks);
        }
        return patrimonio;
    }

    /*
    * CalculateGains calculates how much each contribution will evaluate
    * @param aporteInicial -> amount of moneyz per week 
    * @param tempoSemanas -> number of weeks awaiting for moneyz to increase
    */
    public static double calculateGains(double aporteInicial, int tempoSemanas ){
        // M = P * (1 + i)^t/252
        // M = aporteInicial * (1 + i)^tempoEmdias/252.
        int tempoEmdias = tempoSemanas * diasPorSemana; // semanas e dias por semana
        
        double ret = 0;
        
        ret = aporteInicial * ( Math.pow( (1 + rendimento), ((double)tempoEmdias/(double)diasUteisNoAno)));
       
        return ret;
    }
    /*
    * static round -> rounds the double x.xxxxxxxxxxxxx to x.xx, used for displaying currency
    * @param aporteInicial -> amount of moneyz per week 
    * @param tempoSemanas -> number of weeks awaiting for moneyz to increase
    */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
    
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}