# Desafio Diin

Projeto-desafio da Diin, fintech.

# Instruções

## Download docker image macknighter/desafiodiin
## docker run macknighter/desafiodiin
## visitar http://172.17.0.2:8080/

> Existem duas opções nesta tela:
> Testar a situação do João (específicado no enunciado do desafio).
> Testar uma situação genérica.

## Também é possível fazer o download do projeto no bitbucket
## git clone https://bitbucket.org/Macknight/desafio-diin.git
## Neste caso é mais fácil rodar o jar diretamente: 
> * cd {raiz-do-projeto}/build/libs
> * java -jar gs-serving-web-content-0.1.0.jar